const {Client} = require('pg');
const assert = require('assert');

const client = new Client({
   user: "postgres",
   password: "qwerty",
   host: "localhost",
   port: 5432,
   database: "task28"
});

async function create() {
    try {
        await client.connect()
        console.log("Client connected Successfully!");
        const text = 'INSERT INTO data (name, surname, age, phone) VALUES ($1, $2, $3, $4) RETURNING *';
        const info = await client.query(text, ['Bob', 'Tom', 23, '+79115721891']);
        console.table(info.rows);
        assert.strictEqual(info.rowCount, 1, 'Ошибка при вставке данных: количество строк не соответствует ожидаемому');
    } catch (error){
        console.error(error);
    } finally {
        await client.end();
        console.log('Client disconnected successfully!');
    }
}

async function get() {
    try {
        await client.connect()
        console.log("Client connected Successfully!");
        const text = 'SELECT * FROM data';
        const info = await client.query(text);
        console.table(info.rows);
        assert.ok(info.rowCount > 0, 'Ошибка при получении данных: данные отсутствуют');
    } catch (error){
        console.error(error);
    } finally {
        await client.end();
        console.log('Client disconnected successfully!');
    }
}

async function update() {
    try {
        await client.connect()
        console.log("Client connected Successfully!");
        const text = 'UPDATE data SET name = $1, age = $2 WHERE id = $3 RETURNING *';
        const info = await client.query(text, ['Jon', 12, 2]);
        console.table(info.rows[0]);
        assert.strictEqual(info.rowCount, 1, 'Ошибка при обновлении данных: количество обновленных строк не соответствует ожидаемому');
    } catch (error){
        console.error(error);
    } finally {
        await client.end();
        console.log('Client disconnected successfully!');
    }
}

async function Delete() {
    try {
        await client.connect()
        console.log("Client connected Successfully!");
        const text = 'DELETE FROM data WHERE id = $1 RETURNING *';
        const info = await client.query(text, [2]);
        console.table(info.rows[0]);
        assert.strictEqual(info.rowCount, 1, 'Ошибка при удалении данных: количество удаленных строк не соответствует ожидаемому');
    } catch (error){
        console.error(error);
    } finally {
        await client.end();
        console.log('Client disconnected successfully!');
    }
}

create().then(() => {
    console.log("create() выполнено успешно");
}).catch(error => {
    console.error(error);
});

get().then(() => {
    console.log("get() выполнено успешно");
}).catch(error => {
    console.error(error);
});

update().then(() => {
    console.log("update() выполнено успешно");
}).catch(error => {
    console.error(error);
});

Delete().then(() => {
    console.log("delete() выполнено успешно");
}).catch(error => {
    console.error(error);
});
